---
title: "Coding Dojo"
date: 2018-12-21
---

# Bienvenue sur le site Coding Dojo

L'objectif de ce site est de regrouper et partager un ensemble de ressources sur les coding dojo et le software craftsmanship.
Vous trouverez donc ici des descriptions de [kata](/fr/kata/), des pages de [groupes locaux](/dojo/) et des [descriptions de session](/record/).

N'hésitez pas à enrichir le contenu avec par exemple vos sessions, vos groupes, vos idées de katas ou vos traductions de katas.
